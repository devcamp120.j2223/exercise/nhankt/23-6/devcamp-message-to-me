import { Component } from "react";

import TitleText from "./text/TitleText";
import TitleImage from "./image/TitleImage";

class TitleComponent extends Component {
    render() {
        return (
            <div>
                <TitleText />
                <TitleImage />
            </div>
        )
    }
}

export default TitleComponent;